var keys = require('./keys.js');
var Twitter = require('twitter');

var client = new Twitter(keys);

module.exports = {
	getTimeline: function(params, callback) {
		client.get('statuses/user_timeline', params, callback);
	},
	sendTweet: function(text, callback) {
		client.post('statuses/update', {status: text, trim_user: true}, callback);
	},
	sendDM: function(screenName, messageText, callback) {
		client.post('/direct_messages/new',{screen_name: screenName, text: messageText},callback);
	}
}
