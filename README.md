#Foxxbot#
This is the code behind the [OMGItsFoxxbot](https://twitter.com/omgitsfoxxbot) Twitter bot. It takes tweets from [OMGItsFirefoxx](https://twitter.com/omgitsfirefoxx), puts them into a Markov Chain, and then tweets out every hour at 45 minutes past

### Setup ###
**1 Install node and npm**
See [this guide](https://nodejs.org/en/download/package-manager/) for how to do that

**2 Clone this repository and install dependancies**
`git clone https://bitbucket.org/lukemoll/foxxbot.git`
(inside foxxbot/)
`npm install`
**3 Create a Twitter App**
Go to [https://apps.twitter.com](https://apps.twitter.com) and create a new app. You may need to link a mobile phone number to your account to do this.

**4 Create keys.js**
The Consumer Key, Consumer Secret, Access Token, and Access Secret are stored in `keys.js`, omitted for obvious reasons. Copy these from your Twitter app details into `keys.js` as so:

```
#!js
module.exports = {
        consumer_key: "consumer key here",
        consumer_secret: "consumer secret here",
        access_token_key: "access token here",
        access_token_secret: "access secret here"
};

```
**5 Change target twitter account**
In `refreshcorpus.js, you need to change the target twitter account in **two places**, on lines **21** and **32** from
```
#!js

tf.getTimeline({screen_name: "omgitsfirefoxx", ...
```
to

```
#!js

tf.getTimeline({screen_name: "targetaccount", 
```
for analysing tweets from @targetaccount.

**6 Set up cron**
Use `crontab -e` (on Linux) to access your crontab. I used `45 * * * * /path/to/node /path/to/foxxbot/scheduledtweet.js` to tweet every hour at 45 minutes past. Have a look at [crontab.guru](https://crontab.guru/) if you'd like a different timing.
Make sure you've run `node refreshcorpus.js` at least once before you run `scheduledtweet.js`, you can automate `refreshcorpus.js` as well, if you'd like

### Questions? ###
Send me a tweet: [@LukeMoll_](https://twitter.com/lukemoll_)