var MarkovChain = require('markovchain');
var fs = require('fs');

var quotes = new MarkovChain();
quotes.wordBank = JSON.parse(fs.readFileSync('./wordBank.json','utf-8'));

var randomWord = function(wordList) {
	var tmpList = Object.keys(wordList);
	return tmpList[~~(Math.random() * tmpList.length)];
}

module.exports = {
	getRandomSentence: function(lengthFn) {
		return quotes.start(randomWord).end(lengthFn).process();
	}
}
