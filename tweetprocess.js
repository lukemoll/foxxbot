
var URLregex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
var hashtagRegex = /#[a-z](?:[a-z\d]+)/g;
var screenNameRegex = /@[_a-z\d]+/g;
var undesiredCharsRegex = /[^a-z\s!?,'0-9.]/g;

module.exports = {
	processRawTweet: function(tweet) {
	        	var processedTweet;
	        	processedTweet = tweet.toLowerCase().replace(URLregex, "").replace(hashtagRegex,"").replace(screenNameRegex,"").replace(undesiredCharsRegex,"");
			return processedTweet;
		}

}

