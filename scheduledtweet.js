var tf = require('./twitterfuncs.js')
var quotes = require('./quotes.js');
var endfuncs = require('./endfuncs.js');
var fs = require('fs');

var text = quotes.getRandomSentence(endfuncs.chars(80)).trim();

while(text.length > 140) {
	text = text.replace(/\s\S+$/,"");
}
console.log(text + " [" + text.length + "]");

tf.sendTweet(text,(err,data,response) => {
	if(err) {
		fs.open('scheduled.err','a',(err1,fd) => {
			if(!err1) {
				fs.writeSync(fd,err);
			}
		});
		throw err;
	}
});

