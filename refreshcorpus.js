var tf = require('./twitterfuncs.js');
var fs = require('fs');
var tweetprocess = require('./tweetprocess.js');
var MarkovChain = require('markovchain');

tweets = [];

var depth = 0;

recurFetch = function(error, data, response) {
	depth++;
	if(data.length > 0) {
		for(var i=0, len=data.length;i<len;i++) {
        	        tweets.push(tweetprocess.processRawTweet(data[i].text));
        	}
		var maxid = data[data.length-1].id;

		console.log("depth: " + depth + "\tlength: " + data.length + "\ttotal: " + tweets.length);

		if(depth < 20) {
                	tf.getTimeline({screen_name: "omgitsfirefoxx", count: 200, max_id: maxid, include_rts: false, trim_user: true}, recurFetch);
        	}
		else {
			whenDone();
		}
	}
	else {
		whenDone();
	}
};

tf.getTimeline({screen_name: "omgitsfirefoxx", count: 200, include_rts: false, trim_user: true}, recurFetch);

whenDone =  function() {
	tweets.forEach((val,i,arr) => {if(val.length == 0) {arr.splice(i,1)};});
	fs.open('corpus.txt','w',(err,fd) => {
		for(var i=0;i<tweets.length;i++) {
			fs.writeSync(fd,tweets[i] + "\n");
		}
		console.log("Wrote corpus.txt");
		var markov = new MarkovChain(fs.readFileSync('corpus.txt','utf-8'));
		fs.open('wordBank.json','w',(err1,fd1) => {
			fs.writeSync(fd1,JSON.stringify(markov.wordBank));
			console.log("Wrote wordBank.json");
		});
		
	});
	var tweetlengths = tweets.map((ele,i,array) => ele.length);
	fs.open('data/tweetlengths.json','w',(err,fd2) => {
		if(!err) {
			var lengthobj = {lengths: tweetlengths};
			fs.writeSync(fd2,JSON.stringify(lengthobj));
			console.log("Wrote data/tweetlengths.json");
		}
		else throw err;
	});

}
